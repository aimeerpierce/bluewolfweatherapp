## Bluewolf Weather Application

The Bluewolf Weather Application was developed with a MEAN stack: MongoDB, Express, AngularJS, and Node.js.

Source Files are organized as follows:  

app  
--routes.js  
--cb.js  
public  
--js  
----app.js  
----approutes.js  
----controllers  
------MainCtrl.js  
--css  
----style.css  
--views  
----index.html  
--libs  
server.js  
package.json  
package-lock.json  
bower.json  
README.md  


